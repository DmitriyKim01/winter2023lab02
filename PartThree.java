import java.util.Scanner;

public class PartThree {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        //User Input:
        System.out.println("Enter your first number: ");
        int num1 = scan.nextInt();
        System.out.println("Enter your second number: ");
        int num2 = scan.nextInt();

        //Static:
        System.out.println("Addition:");
        System.out.println(num1 + " + " + num2 + " is equal to: " + Calculator.add(num1, num2));
        System.out.println("Substraction:");
        System.out.println(num1 + " - " + num2 + " is equal to: " + Calculator.substract(num1, num2));

        //Non Static:
        Calculator calculator = new Calculator();
        System.out.println("Multiplication:");
        System.out.println(num1 + " * " + num2 + " is equal to: " + calculator.multiply(num1, num2));
        System.out.println("Division:");
        System.out.println(num1 + " / " + num2 + " is equal to: " + calculator.divide(num1, num2));
    }
}
