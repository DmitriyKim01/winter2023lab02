public class MethodsTest{
    public static void main(String[] args){
        int x =5;

        //Question 1-4
        System.out.println("Question 1-4:");
        System.out.println("The value of x in the main: " + x);
        methodNoInputNoReturn();
        System.out.println("The value of x in the main (after calling  methodNoInputNoReturn): " + x +"\n");

        //Question 5
        System.out.println("Question 5:");
        System.out.println("The value of x in the main: " + x);
        methodOneInputNoReturn(x+10);
        System.out.println("The value of x in the main (after calling  methodOneInputNoReturn): " + x +"\n") ;

        //Question 6
        System.out.println("Question 6:");
        methodTwoInputNoReturn(30, 45);
        System.out.println();

        //Question 7
        System.out.println("Question 7:");
        int z = methodNoInputReturnInt();
        System.out.println("The value of the methodNoInputReturnInt() is: " + z +"\n");

        //Question  8-9
        System.out.println("Question 8-9:");
        double root = sumSquareRoot(9, 5);
        System.out.println("The square root of 5 + 9 is: " + root+"\n");

        //Question 10
        System.out.println("Question 10:");
        String s1 = "java";
        String s2 = "programming";
        System.out.println("The length of the s1: " + s1.length());
        System.out.println("The length of the s2: " + s2.length() + "\n");

        //Question 11:
        System.out.println("Question 11:");
        System.out.print("The result of the adddOne Method: ");
        System.out.println(SecondClass.addOne(50));
        SecondClass sc = new SecondClass();
        System.out.print("The result of the adddTwo Method: ");
        System.out.println(sc.addTwo(50));
    }

    //Method from question 1:
    public static void methodNoInputNoReturn(){
        System.out.println("I’m in a method that takes no input and returns nothing" );
        int x = 20;
        System.out.println("The value of x in the methodNoInputNoReturn: "+x);
    }
	//Method from question 5:
    public static void methodOneInputNoReturn(int num){
        System.out.println("Inside the method one input no return" );
        num -= 5;
        System.out.println("The value of x + 10: " + num);
    }
	//Method from question 6:
    public static void methodTwoInputNoReturn(int num, double num2){
        System.out.println("The first number in the methodTwoInputNoReturn: " + num);
        System.out.println("The second number in the methodTwoInputNoReturn: " + num2);
    }
	//Method from question 7:
    public static int methodNoInputReturnInt(){
        return 5;
    }
	//Method from question 8-9:
    public static double sumSquareRoot(int num, int num2){
        double sum = num + num2;
        return Math.sqrt(sum);
    }
}